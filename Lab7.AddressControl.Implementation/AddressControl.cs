﻿using Lab7.AddressControl.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7.AddressControl
{
   public class AddressControl:IAddress
    {
        public System.Windows.Controls.Control Control
        {
            get {
                return new System.Windows.Controls.Control();
            }
        }

        public event EventHandler<AddressChangedArgs> AddressChanged;
    }
}
