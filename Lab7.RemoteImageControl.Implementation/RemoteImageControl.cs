﻿using Lab7.AddressControl.Contract;
using Lab7.RemoteImageControl.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Lab7.RemoteImageControl.Implementation
{
   public class RemoteImageControl:IRemoteImage
    {
       private IAddress address;
       private Control controll;


       public RemoteImageControl(IAddress address)
       {
           this.address = address;
           this.controll = new Control();
       }

        public System.Windows.Controls.Control Control
        {
            get { return controll; }
        }

        public void Load(string url)
        {
            throw new NotImplementedException();
        }
    }
}
